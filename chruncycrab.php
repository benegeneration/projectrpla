<?php

include_once("header1.php")
?>

<style>
.card {
  box-shadow: 4px 6px 10px 0 rgba(0, 1, 4, 0.2);
  max-width: 400px;
 
  margin: auto;
  text-align: center;
  font-family: arial;
}

.title {
  color: grey;
  font-size: 20px;
}

button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

a {
  text-decoration: none;
  font-size: 22px;
  color: black;
}

button:hover, a:hover {
  opacity: 0.7;
}
</style>
</head>
<body>
 
 <br>
 
 <center>
 <div class="row">
     <?php
     
    include("koneksi.php");
    $limit = 6;  
    if (isset($_GET["page"])) { 
        $page  = $_GET["page"]; 
    } else { 
        $page=1; 
    };
    $start_from = ($page-1) * $limit;  
    $sql="SELECT nama_produk,harga,foto_produk FROM chrunchy ORDER BY nama_produk ASC LIMIT ?,?";
    $stmt = $kon->prepare($sql);
    $stmt->bind_param("ii", $start_from, $limit);
    $stmt->execute();
    $stmt->bind_result($nama_produk,$harga,$foto_produk);
 
 ?>
    <?php



   while($stmt->fetch()){ ?>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="zoom"> 
        <div class="card h-100">
		<img  src='./admin2/fotoChrunchy/<?php echo $foto_produk;?>'style="width:100%;height:250px">   
    <div class="card-body">
            <h4 class="card-title">
              <a><?php echo $nama_produk; ?></a>
            </h4>
            <h5><?php echo $harga;?></h5>
 
          </div>
         
        </div>
          </div>
      </div>  
      <?php }?>
    </div>
 
    <!-- /.row -->
   
    <?php 
           $stmt->close();
            
            $sqlpaging = "SELECT COUNT(kode_produk) FROM chrunchy";  
            $result = $kon->query($sqlpaging);
            $row = $result->fetch_row();  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);  
            $pagLink = "<ul class='pagination ' >";  
            for ($i=1; $i<=$total_pages; $i++) {  
                         $pagLink .= "<li class='page-item'><a class='page-link' href='chrunchycrab.php?page=".$i."' >".$i."</a></li>";  
            };  
            echo $pagLink . "</ul>";  

            $kon->close();
        ?>
 

	</center>
	<br><br>

<?php

include_once("footer1.php");

?>