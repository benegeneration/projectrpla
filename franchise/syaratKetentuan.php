<?php
include_once("headerpage.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
<style>
    .f{
        font-size:20px;
       text-align:justify;
    }

</style>

</head>
<body>
    

<div class="row">
    <div class="col-9">
<ol >
<h2 style="font-weight:bold"> Syarat dan Ketentuan Franchise</h2><br>
<li class="f">Pembayaran 100% di muka jika perjanjian kontrak disetujui.</li>
<li  class="f">Royalty sebanyak 1.5% dari omset penjualan.</li>
<li  class="f">Setiap cabang hanya boleh melakukan supply bahan / pembelian bahan melalui Master CHIO Tea.</li>
<li  class="f">Tidak diperkenankan untuk melakukan supply bahan dari supplier lain selama kontrak berjalan.</li>
<li  class="f">Untuk menjaga pencitraan nama dan merk 'CHIO Tea', Penyalahgunaan bahan ataupun nama merk 'CHIO Tea' akan
dikenakan sanksi pemberhentian kontrak secara sepihak tanpa pengembalian dana dan pengecualian apapun.</li>
<li  class="f">Tidak diperbolehkan untuk menjual merk 'CHIO Tea' di tempat lain selain daripada lokasi yang telah disepakati dalam
kontrak perjanjian.</li>
<li  class="f">Harga Penjualan segala menu minuman CHIO Tea harus disesuaikan dengan harga jual yang telah ditentukan dari Master
CHIO Tea.</li>
<li  class="f">Perubahan menu harus disesuaikan dengan menu Master CHIO Tea.</li>
<li class="f">Jumlah trainee yang mengikuti training pembuatan menu minuman berjumlah maksimal 4 orang.</li>
<li class="f">Tidak diperbolehkan memberikan atau menentukan diskon / promosi sendiri. Semua diskon / promosi yang diberikan harus
dengan persetujuan atau diskusi terlebih dahulu dengan Master CHIO Tea.</li>
<li class="f">Segala design dan branding CHIO Tea menggunakan jasa design EDGE Creative Team. Untuk melakukan branding dan
percetakan sendiri, maka file yang akan diambil dari EDGE Creative Team akan dikenakan biaya design.</li>
<li class="f">Proses memakan waktu ± 2 - 3 bulan untuk penyerahan bahan dan peralatan, mempertimbangkan kondisi dan lokasi dari
booth yang akan dibuka.</li>
<li class="f">Ukuran booth CHIO Tea ± 7 m2.</li>

</ol>
</div>
  <div class="col-3">
        <img style="width:200px;height:300px;float:right;margin-right:20px" src="chio.jpg" >
        </div>

</div>
</body>
</html>
<?php

include_once("footerpage.php");

?>