<?php
  include_once("headerpage.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="style.css">
<style>
html {
  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}

</style>
</head>
<body>
<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Home</a>
        </li>
        <li class="breadcrumb-item active">CHIO TEA AND COFFEE</li>
</ol>
      <div class="row">
        <div class="col-8">
 
          <h1>CHIO TEA AND COFFEE</h1>
 
          <p>Data Paket Franchise Chio Tea and Coffee</p>
        
</div>  <div class="col-4">
        <img style="width:100px;height:150px;float:right;margin-right:20px" src="chio.jpg" >
        </div>

        </div>
<br>
      
<div class="container">
<div class="row">

  <div class="column">
    <div class="card">
      <img src="chio1.jpg"   style="width:100%">
      <div class="container">
        <h2>Paket Franchise 1</h2>
        <p class="title">500 Cup</p>
        <p>Design Booth Rp60.000.000,-/3 tahun</p>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="chio6.jpg"  style="width:100%">
      <div class="container">
        <h2>Paket Franchise 2</h2>
        <p class="title">1000 Cup</p>
        <p>Design Booth Rp90.000.000,-/3 tahun</p>

      </div>
    </div>
  </div>
  <div class="column">
    <div class="card">
      <img src="chio7.jpg"  style="width:100%">
      <div class="container">
        <h2>Paket Franchise 3</h2>
        <p class="title">1500 Cup</p>
        <p>Design Booth Rp120.000.000,-/3 tahun</p>
      </div>
    </div>
  </div>
</div>
</div><br><br>

</body>
</html>

<?php
include_once("footerpage.php");
?>