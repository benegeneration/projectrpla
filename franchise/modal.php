<?php
include_once("headerpage.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Modal Menu Minuman</h2>
  <p>Modal menu minuman Chio Tea</p>   
  <div class="row">
    <div class="col-10">
  <table class="table table-hover">
    <thead>
      <tr>
        <th>No</th>
        <th>Menu</th>
        <th>Biaya /cup(Rp)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Cheese Cream Chocolate</td>
        <td>12.428</td>
      </tr>
      <tr>
        <td>2</td>
        <td>Cheese Cream Taro</td>
        <td>12.428</td>
      </tr>
      <tr>
        <td>3</td>
        <td>Cheese Cream Matcha</td>
        <td>12.428</td>
      </tr>
      <tr>
        <td>4</td>
        <td>Cheese Cream Milo</td>
        <td>12.428</td>
      </tr>
      <tr>
        <td>5</td>
        <td>Cheese Cream Milk Tea</td>
        <td>12.428</td>
      </tr>
      <tr>
        <td>6</td>
        <td>Cheese Cream Red Velvet</td>
         <td>12.578</td>
      </tr>
      <tr>
        <td>7</td>
        <td>Cheese Cream Jasmine Tea</td>
        <td>11.028</td>
      </tr>
      <tr>
        <td>8</td>
        <td>Cheese Cream Green Milk Tea</td>
        <td>12.628</td>
      </tr>
      <tr>
        <td>9</td>
        <td>Cheese Cream Roselle Tea</td>
        <td>12.428</td>
      </tr>
      <tr>
        <td>10</td>
        <td>Cheese Cream Thai Tea</td>
        <td>12.628</td>
      </tr>
      <tr>
        <td>11</td>
        <td>Tropical Lemon Jasmine Tea</td>
        <td>6.446</td>
      </tr>
      <tr>
        <td>12</td>
        <td>Tropical Lemon Green Tea</td>
        <td>6.446</td>
      </tr>
      <tr>
        <td>13</td>
        <td>Chio Coffee</td>
        <td>8.346</td>
      </tr>
      <tr>
        <td>14</td>
        <td>Vita Lemon C</td>
        <td>7.586</td>
      </tr>
      <tr>
        <td>15</td>
        <td>Royal Coco Puding</td>
        <td>6.046</td>
      </tr>
    </tbody>
  </table>
<br><br>
<h2>Modal Menu Topping</h2>
<p>Modal Menu Topping Chio Tea</p>
<br>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>No</th>
        <th>Menu Topping</th>
        <th>Biaya /cup(Rp)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Black Bubble</td>
        <td>250</td>
      </tr>
      <tr>
        <td>2</td>
        <td>Golden Bubble</td>
        <td>300</td>
      </tr>
      <tr>
        <td>3</td>
        <td>Oreo</td>
        <td>600</td>
      </tr>
      <tr>
        <td>4</td>
        <td>Grass Jelly</td>
        <td>300</td>
      </tr>
      <tr>
        <td>5</td>
        <td>Mango Pudding</td>
        <td>200</td>
      </tr>
      <tr>
        <td>6</td>
        <td>Coconut Jelly</td>
         <td>1.000</td>
      </tr>
      <tr>
        <td>7</td>
        <td>Cheese Cream</td>
        <td>6.582</td>
      </tr>
      <tr>
        <td>8</td>
        <td>Coffee Jelly</td>
        <td>1.000</td>
      </tr>
      <tr>
        <td>9</td>
        <td>Rainbow Jelly</td>
        <td>1.000</td>
      </tr>
      <tr>
        <td>10</td>
        <td>Basil Seed</td>
        <td>1.000</td>
      </tr>
      </tbody>
  </table>
</div>
<div class="col-2">
        <img style="width:120px;height:200px;float:right;margin-right:20px" src="chio.jpg" >
        </div>
</div>
    

  </div>   <br>
</body>
</html>


<?php
include_once("footerpage.php");
?>