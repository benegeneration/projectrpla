
<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="logo.jpg">
  <title>Waralaba Adi Muda Jaya</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-info fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.php"  style="color:black ; font-weight:bold">Waralaba Adi Muda Jaya</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">
    <?php if(isset($_SESSION["username"])) { ?>
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="circle">
          <a class="nav-link" href="index.php">
            <i class="fa fa-fw fa-area-circle"></i>
            <span class="nav-link-text" style=" font-weight:bold;font-size:20px;color:white">Home</span>
          </a>
        </li>
        
           <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Chio">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseChio" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-square"></i>
            <span class="nav-link-text"  style=" font-weight:bold;color:white">CHIO TEA AND COFFEE</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseChio">
            <li>
              <a href="paket.php"  style="color:white">Paket Franchise</a>
            </li>
            <li>
              <a href="modal.php" style="color:white">Harga Modal</a>
            </li>
            <li>
              <a href="syaratKetentuan.php" style="color:white">Syarat Dan Ketentuan</a>
            </li>
          </ul>
        </li>
      
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="chrunchycrab">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapsechrunchycrab" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-square"></i>
            <span class="nav-link-text" style=" font-weight:bold;color:white">CHRUNCHY CRAB</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapsechrunchycrab">
            <li>
              <a href="paket.php" style="color:white">Paket Franchise</a>
            </li>
            <li>
              <a href="modal.php" style="color:white">Harga Modal</a>
            </li>
            <li>
              <a href="syaratKetentuan.php" style="color:white">Syarat Dan Ketentuan</a>
            </li>
          </ul>
        </li>
      
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="SushiGo">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseSushiGo" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-square"></i>
            <span class="nav-link-text " style=" font-weight:bold;color:white">SUSHI GO</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseSushiGo">
            <li>
              <a href="paket.php" style="color:white">Paket Franchise</a>
            </li>
            <li>
              <a href="modal.php" style="color:white">Harga Modal</a>
            </li>
            <li>
              <a href="syaratKetentuan.php" style="color:white">Syarat Dan Ketentuan</a>
            </li>
          </ul>
        </li>
     
      </ul>
      </ul>
<?php } ?>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
     

         
        
         <?php if(isset($_SESSION["username"])) { ?>
          <li class="nav-item">
          <a class="nav-link bg-info"  style="color:black"><?php echo "Welcome: ". $_SESSION["username"]; ?></a>
          </li>
        <li class="nav-item">
          <a class="nav-link bg-info" href="../logout.php"  style="color:black">
            <i class="fa fa-fw fa-sign-out"  style="color:black"></i>Logout</a>
        </li>
        <?php } else { ?>
       
          <li class="nav-item">
            <a class="nav-link bg-info" href="../index.php"  style="color:black"> 
            <i class="fa fa-fw fa-sign-out"  style="color:black"></i>Login</a>
          </li>
          <li>
          <a class="nav-link bg-info" href="../home1.php"  style="color:black">
            <i    style="color:black"></i>Home AMJ</a>
      
          </li>
        <?php } ?>
</ul>   

       
    </div>
    
  </nav>
  
  <div class="content-wrapper">
    <div class="container-fluid">
    