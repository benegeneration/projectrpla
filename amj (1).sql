-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2018 at 05:14 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amj`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminamj`
--

CREATE TABLE `adminamj` (
  `id_admin` varchar(15) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `passwordamj` varchar(12) NOT NULL,
  `jabatan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminamj`
--

INSERT INTO `adminamj` (`id_admin`, `nama_admin`, `username`, `passwordamj`, `jabatan`) VALUES
('admin1', 'I Putu Krisna Bayu', 'adminamj', 'adminpunya', 'admin'),
('franchise01', 'Otniel Edward', 'frch_otniel', 'sanskaubene', 'franchise');

-- --------------------------------------------------------

--
-- Table structure for table `franchise`
--

CREATE TABLE `franchise` (
  `id_franchise` varchar(15) NOT NULL,
  `nama_franchise` varchar(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `passwordfrch` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `wilayah_franchise` varchar(25) NOT NULL,
  `id_admin` char(10) NOT NULL,
  `jabatan` varchar(12) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_produk`
--

CREATE TABLE `kategori_produk` (
  `id_admin` char(10) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_produk`
--

INSERT INTO `kategori_produk` (`id_admin`, `id_kategori`, `nama_kategori`) VALUES
('admin1', 'CHT01', 'Chio Tea and Coffe');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_admin` char(10) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `kode_produk` char(8) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `harga_produk` bigint(11) NOT NULL DEFAULT '0',
  `foto_produk` varchar(200) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_admin`, `id_kategori`, `kode_produk`, `nama_produk`, `harga_produk`, `foto_produk`, `tanggal`) VALUES
('admin1', 'cht01', 'ccttct01', 'Cheese Cream Thai Teaa', 28000, '5ae4cd0c99c52AMJ.jpg', '2018-04-29 02:35:40'),
('admin1', 'cht01', 'ccttct02', 'Cheese Cream Jasmine Teaa 2', 30000, '5ae56fc706051Capgfhfgture.PNG', '2018-04-29 14:09:59'),
('admin1', 'cht01', 'ccttct03', 'Cheese Cream Jasminee Teaaa', 30000, '5ae5556689be7Cadfgfdgpture.PNG', '2018-04-29 12:17:26'),
('admin1', 'cht01', 'ccttct04', 'Cheese Cream Jasmine Teaa 4', 24000, '5ae56f9ff081cCaptfgdfgure.PNG', '2018-04-29 14:09:19'),
('admin1', 'cht01', 'ccttct05', 'Cheese Cream Jasmine Teaa 5', 25000, '5ae56fe2ccf44avatar2.png', '2018-04-29 14:10:26'),
('admin1', 'cht01', 'ccttct06', 'Teh aja', 4000, '', '2018-04-30 15:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `promo`
--

CREATE TABLE `promo` (
  `id_promo` varchar(10) NOT NULL,
  `id_admin` char(10) NOT NULL,
  `foto_promo` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(255) NOT NULL,
  `kode_produk` int(11) NOT NULL,
  `vote` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminamj`
--
ALTER TABLE `adminamj`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `franchise`
--
ALTER TABLE `franchise`
  ADD PRIMARY KEY (`id_franchise`);

--
-- Indexes for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`kode_produk`);

--
-- Indexes for table `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id_promo`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
