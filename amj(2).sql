-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2018 at 10:51 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amj`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminamj`
--

CREATE TABLE `adminamj` (
  `id_admin` varchar(15) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `passwordamj` varchar(12) NOT NULL,
  `jabatan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminamj`
--

INSERT INTO `adminamj` (`id_admin`, `nama_admin`, `username`, `passwordamj`, `jabatan`) VALUES
('admin1', 'I Putu Krisna Bayu', 'adminamj', 'adminpunya', 'admin'),
('franchise01', 'Otniel Edward', 'frch_otniel', 'sanskaubene', 'franchise');

-- --------------------------------------------------------

--
-- Table structure for table `chio`
--

CREATE TABLE `chio` (
  `id_admin` varchar(12) NOT NULL,
  `kode_produk` varchar(12) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `harga` varchar(50) NOT NULL DEFAULT 'Awali dengan Rp.',
  `foto_produk` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chio`
--

INSERT INTO `chio` (`id_admin`, `kode_produk`, `nama_produk`, `harga`, `foto_produk`, `tanggal`) VALUES
('admin1', 'CTCCBT', 'Cheese Cream Black Tea', 'Rp.29.000,00', '5aefcef4a3b8fCCBT.JPG', '2018-05-07 10:58:44'),
('admin1', 'CTCCF', 'Chio Coffee', 'Rp.22.000,00', '5aefd03868f5bCCF.JPG', '2018-05-07 11:04:08'),
('admin1', 'CTCCGMT', 'Cheese Cream Green Milk Tea', 'Rp.29.000,00', '5aefcf2b683b8CCGMT.JPG', '2018-05-07 10:59:39'),
('admin1', 'CTCCJT', 'Cheese Cream Jasmine Tea', 'Rp.27.000,00', '5aefceb7b6303CCJT.JPG', '2018-05-07 10:57:43'),
('admin1', 'CTCCTT', 'Cheese Cream Thai Tea', 'Rp.28.000,00', '5aefce64a2eb2CCTT.jpg', '2018-05-07 10:56:20'),
('admin1', 'CTTLGT ', 'Tropical Lemon Green Tea', 'Rp.23.000,00', '5aefcfb5e46e7TLGT.JPG', '2018-05-07 11:01:57'),
('admin1', 'CTTLJT', 'Tropical Lemon Jasmine Tea', 'Rp.23.000,00', '5aefcff5e84ceTLJT.JPG', '2018-05-07 11:03:01'),
('admin1', 'CTVLC', 'Vita Lemon C', 'Rp.23.000,00', '5aefcf6525305VLC.JPG', '2018-05-07 11:00:37');

-- --------------------------------------------------------

--
-- Table structure for table `chrunchy`
--

CREATE TABLE `chrunchy` (
  `id_admin` varchar(12) NOT NULL,
  `kode_produk` varchar(12) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `harga` varchar(50) NOT NULL DEFAULT '0',
  `foto_produk` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chrunchy`
--

INSERT INTO `chrunchy` (`id_admin`, `kode_produk`, `nama_produk`, `harga`, `foto_produk`, `tanggal`) VALUES
('admin1', 'ccttct02', 'Cheese Cream Jasmine Tea', 'Rp.28.000,00', '5aefd17e712a9avatar2.png', '2018-05-07 11:09:34');

-- --------------------------------------------------------

--
-- Table structure for table `franchise`
--

CREATE TABLE `franchise` (
  `id_franchise` varchar(15) NOT NULL,
  `nama_franchise` varchar(100) NOT NULL,
  `username` varchar(20) NOT NULL,
  `passwordfrch` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `wilayah_franchise` varchar(25) NOT NULL,
  `id_admin` char(10) NOT NULL,
  `jabatan` varchar(12) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `franchise`
--

INSERT INTO `franchise` (`id_franchise`, `nama_franchise`, `username`, `passwordfrch`, `email`, `alamat`, `no_telp`, `wilayah_franchise`, `id_admin`, `jabatan`, `date`) VALUES
('franchise01', 'Otniel Edward M G', 'edward_otniel', 'c645d043789083305893', 'goeltom@gmail.com', 'Jalan.Wahid Hasyim', '081372357583', 'Yogyakarta', 'admin1', 'franchise', '2018-05-06 00:37:52'),
('franchise02', 'Otniel Edward Marganti Gultom', 'edward_otniel', 'c645d043789083305893', 'edwardotniel27@gmail.com', 'Jalan.Wahid Hasyim', '081345789125', 'Yogyakarta', 'admin1', 'franchise', '2018-05-06 00:38:56');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `nama` varchar(100) NOT NULL,
  `email` varchar(70) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `perihal` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`nama`, `email`, `no_telp`, `perihal`, `alamat`, `pesan`) VALUES
('Otniel', 'edwardotniel27@gmail.com', '081372357583', 'Produk', 'Jalan.Wahid Hasyim', ' Produk tidak menarik');

-- --------------------------------------------------------

--
-- Table structure for table `promo`
--

CREATE TABLE `promo` (
  `id_promo` varchar(10) NOT NULL,
  `id_admin` char(10) NOT NULL,
  `foto_promo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promo`
--

INSERT INTO `promo` (`id_promo`, `id_admin`, `foto_promo`) VALUES
('promo5', 'admin1', '5aefd6c9970e6IMG-5534.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `sushigo`
--

CREATE TABLE `sushigo` (
  `kode_produk` varchar(12) NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `harga` varchar(50) NOT NULL DEFAULT '0',
  `foto_produk` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sushigo`
--

INSERT INTO `sushigo` (`kode_produk`, `id_admin`, `nama_produk`, `harga`, `foto_produk`, `tanggal`) VALUES
('SGBCG', 'admin1', 'Black Caviar Gunkan', 'Rp.5.000,00', '5aefc06006195BlackCaviarGunkan.JPG', '2018-05-07 09:56:32'),
('SGCFG', 'admin1', 'Chicken Floss Gunkan', 'Rp.7.000,00', '5aefc28443d3aChickenFlossGunkan.JPG', '2018-05-07 10:05:40'),
('SGCR', 'admin1', 'California Roll', 'Rp.4.500,00', '5aefc238a5649CaliforniaRoll.JPG', '2018-05-07 10:04:24'),
('SGIBC', 'admin1', 'Inari Black Caviar', 'Rp.6.000,00', '5aefc2c7bb85eInariBlackCaviar.JPG', '2018-05-07 10:06:47'),
('SGIGT', 'admin1', 'Inari Green Tobiko', 'Rp.6.000,00', '5aefc30068f75InariGreenTobiko.JPG', '2018-05-07 10:07:44'),
('SGIKCM', 'admin1', 'Inari Kani Corn Mayo', 'Rp.6.000,00', '5aefc3365cd33InariKaniCornMayo.JPG', '2018-05-07 10:08:38'),
('SGIOT', 'admin1', 'Inari Orange Tobiko', 'Rp.6.000,00', '5aefc35e94d70InariOrangeTobiko.JPG', '2018-05-07 10:09:18'),
('SGKCG', 'admin1', 'Kani Cheese Gunkan', 'Rp.7.000,00', '5aefc39d4a16bKaniCheeseGunkan.JPG', '2018-05-07 10:10:21'),
('SGKCMG', 'admin1', 'Kani Corn Mayo Gunkan', 'Rp.6.000,00', '5aefc428c94dcKaniCornMayoGunkan.JPG', '2018-05-07 10:12:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminamj`
--
ALTER TABLE `adminamj`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `chio`
--
ALTER TABLE `chio`
  ADD PRIMARY KEY (`kode_produk`);

--
-- Indexes for table `chrunchy`
--
ALTER TABLE `chrunchy`
  ADD PRIMARY KEY (`kode_produk`);

--
-- Indexes for table `franchise`
--
ALTER TABLE `franchise`
  ADD PRIMARY KEY (`id_franchise`);

--
-- Indexes for table `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id_promo`);

--
-- Indexes for table `sushigo`
--
ALTER TABLE `sushigo`
  ADD PRIMARY KEY (`kode_produk`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
