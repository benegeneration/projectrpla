<?php
if(!isset($_SESSION["username"])){

}
else{
  header("Location: /AMJ/index.php");
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
<style>
.dropbtn {
    background-color: black;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}
</style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <title>Document</title>
</head>
<body style = "background-image:url(adminBG3.jpg);background-position:center;background-repeat: no-repeat;background-size:cover;background-attachment: fixed;">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="non-active"><a href="Home.html" style="font-size:20px">Home</a></li>&nbsp
       <li> <div class="dropdown">
                <button class="dropbtn">Insert</button>
                <div class="dropdown-content">
                  <a href="insertProduk.html">Insert Produk</a>
                  <a href="tambahFranchise.php">Insert Franchise</a>
                  <a href="tambahPromo.html">Insert Promo</a>
                 
                </div></div>
                <li>  
                  
                <div class="dropdown">
  <button class="dropbtn">Update</button>
  <div class="dropdown-content">
    <a href="updateProduk.html">Update Produk</a>
    <a href="ubahFranchise.html">Update Franchise</a>
    <a href="ubahPromo.html">Update Promo</a>
  </div>
      </div>
</li>
<li> <div class="dropdown">
  <button class="dropbtn">Delete</button>
  <div class="dropdown-content">
    <a href="deleteProduk.html">Delete Produk</a>
    <a href="hapusFranchise.html">Delete Franchise</a>
    <a href="hapusPromo.html">Delete Promo</a>
    
  </div>
</div>  </li>

       </ul>
       <ul class="nav navbar-nav navbar-right">
          <li >
              <a href="../logout.php" style="font-size:20px">
                  <i class="fa fa-fw fa-sign-out"></i>Logout</a>
          </li>
          </ul>
    </div>
    
  </div>
  
  
</nav>
</body>
</html>