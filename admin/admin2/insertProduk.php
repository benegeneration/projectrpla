<?php
include_once('headerpage.php');
?>

<div class="container">
  <h2>Tambah Produk</h2>
  <p>Pemasukkan data pada database</p>
  <form method="POST" action="prosesinsert.php">
    <div style="width:50%" class="form-group">
      <label for="kode_produk">Kode Produk</label>
      <input class="form-control" name="kode_produk" id="kode_produk" type="text">
    </div>
    <div  style="width:50%"class="form-group">
      <label for="inputlg">Nama Produk</label>
      <input class="form-control input-lg" name="nama_produk" id="nama_produk" type="text">
    </div>
    <div  style="width:50%" class="form-group">
      <label for="inputsm">Id Kategori</label>
      <input class="form-control input-sm" name="id_kategori" id="id_kategori" type="text">
    </div>
    <div  style="width:50%"class="form-group">
      <label for="inputsm">Harga</label>
      <input class="form-control input-sm" name="harga_produk" id="harga_produk" type="text">
    </div>
    <div  style="width:50%"class="form-group">
      <label for="inputsm">Id Admin</label>
      <input class="form-control input-sm" name="id_admin" id="id_admin" type="text">
 <br>
  	  <input type="file" name="image">
  	</div>
 
  	<div>
  		<button style="margin-left:45%" type="submit" name="upload">Tambah</button>
  	</div>
  </form>
      </div>
  </form>
</div>


<?php
include_once('footerpage.php');
?>