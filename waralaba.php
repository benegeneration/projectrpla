<?php

include_once("header1.php");

?>
    <style>
      .s:hover {
        animation: shake 0.5s;
        animation-iteration-count: infinite;
      }
      
      @keyframes shake {
        0% { transform: translate(1px, 1px) rotate(0deg); }
        10% { transform: translate(-1px, -2px) rotate(-1deg); }
        20% { transform: translate(-3px, 0px) rotate(1deg); }
        30% { transform: translate(3px, 2px) rotate(0deg); }
        40% { transform: translate(1px, -1px) rotate(1deg); }
        50% { transform: translate(-1px, 2px) rotate(-1deg); }
        60% { transform: translate(-3px, 1px) rotate(0deg); }
        70% { transform: translate(3px, 1px) rotate(-1deg); }
        80% { transform: translate(-1px, -1px) rotate(1deg); }
        90% { transform: translate(1px, 2px) rotate(0deg); }
        100% { transform: translate(1px, -2px) rotate(-1deg); }
      }
      </style>
<br><br>
 
      <ol class="breadcrumb" style="font-size:18px;font-weight:bold;font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif ; "  >
        <li class="breadcrumb-item">
          <a href="home1.php">Home</a>
		</li>
        <li class="breadcrumb-item active">Waralaba</li>
</ol> <br>
          <div class="row">
              <div class="col-sm-3 col-md-6 col-lg-9">
                <div class=" badge badge-dark" style="width:100%"><h4 class="f">Pengertian Waralaba</h4></div>
              <p align="justify">Franchisor atau Pemberi Waralaba, adalah badan usaha atau perorangan yang memberikan hak kepada pihak lain untuk memanfaatkan dan atau menggunakan hak atas kekayaan intelektual atau penemuan atau ciri khas usaha yang dimilikinya. 
              Franchisor sudah harus siap dengan perlengkapan operasi bisnis dan kinerja manajemen yang baik, menjamin kelangsungan usaha dan distribusi bahan baku untuk jangka panjang,
              serta menyediakan kelengkapan usaha sampai ke detail yang terkecil. Franchisor juga sudah harus menyediakan perhitungan keuntungan yang didapat, neraca keuangan yang mencakup BEP (Break Event Point) dan ROI (Return On Investment).
              Franchisee atau Penerima Waralaba, adalah badan usaha atau perorangan yang diberikan hak untuk memanfaatkan dan atau menggunakan hak atas kekayaan intelektual atau penemuan atau ciri khas yang dimiliki pemberi waralaba. <a href="isiartikel.html">Baca Selengkapnya</a>
              
            </p>
                        
            <div class=" badge badge-dark" style="width:100%"><h4  class="f">Pentingnya Waralaba</h4></div> 
            <p align="justify">
            Memulai usaha selalu menghadapi kendala atau ketidak pastian,begitupun dengan bisnis waralaba meskipun bisnis waralaba tingkat kesuksesan yang lebih tinggi dibandingkan dengan bisnis yang lain ,tetapi bagaimana juga bisnis waralaba memiliki resiko 
            yang namanya kegagalan pula semua tergantung keseriusan serta perencanaan yang matang dalam menjalankan bisnisnya usaha Waralaba adalah bisnis yang telah teruji dan memiliki perencanaan yang baik ,didalam perencaan bisnis waralaba dengan bisnis biasa sangat berbeda jika dalam perencanaan bisnis pada umumnya hanya untuk satu pihak saja
             maka didalam bisnis waralaba perlu perencanaan untuk dua pihak yaitu sebagai franchisor dan sebagai franchisee
            Perencanaan Bisnis waralaba sebagai franchisor adalah perencanaan yang dibuat untuk pemberi waralaba ,sebagai contoh ada beberapa rekan yang memiliki merek begitu terkenal dan memiliki banyak cabang dimana-mana kemudian dengan tanpa perencanaan yang matang 
            si pemilik bisnis tersebut bermaksud mewaralabakan bisnisnya karena banyaknya permintaan untuk membuka cabang dimana-mana dan si pemilik bisnis beranggapan bahwa tanpa perlu perencanaan dia sudah bisa mewaralabakan bisnisnya dan belum memiliki persiapan apa saja yang akan terjadi nantinya .<a href="isiartikel.html">Baca Selengkapnya</a>  

            </p>
            <div class=" badge badge-dark" style="width:100%"><h4 class="f">Pedaftaran Waralaba</h4></div>
            <p align="justify">
            Ingin berwaralaba? Silahkan mengisi data berdasarkan format formulir yang telah tersedia.
            Wilayah operasional Adi Muda Jaya berada di seluruh Indonesia terutama di kota - kota besar di Indonesia. 
            </p><br>
            <div class="panel-footer"><a href ="FormWaralaba.docx" style="text-decoration:none;font-size:16px;font-weight:bold" >Pendaftaran
           </a>   </div>
        
          </div>
          <div class="col-sm-3 col-md-6 col-lg-3">
              <img class="s" src="waralaba.png"  style="width:100%;height:300px">  
            </div>
        </div>
       
  </center><br><br><br>
<?php

include_once("footer1.php");

?>