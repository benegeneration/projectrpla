<?php
include_once("header.php");

?>

<style>
    /* Make the image fully responsive */
    .carousel-inner img {
        width: 50%;
        height: 350px;
        background-image:none;
	}
	
	
img:hover {
    opacity: 0.5;
    filter: alpha(opacity=50); /* For IE8 and earlier */
}

</style>
 
<div class="container">
   <ol class="breadcrumb" style="font-size:18px;font-weight:bold;font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif ; "  >
        <li class="breadcrumb-item">
          <a href="home.php">Home</a>
		</li>
		<li class="breadcrumb-item">
          <a href="produk.php">Produk</a>
        </li>
        <li class="breadcrumb-item active">Sushi GO</li>
</ol> 
 <br>
<center>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
		
<ul class="carousel-indicators">
	<li data-target="#myCarousel" class="active"></li>
	<li data-target="#myCarousel" data-slide-to="1"></li>
	<li data-target="#myCarousel" data-slide-to="2"></li>
	<li data-target="#myCarousel" data-slide-to="3"></li>
	<li data-target="#myCarousel" data-slide-to="4"></li>
	<li data-target="#myCarousel" data-slide-to="5"></li>
	<li data-target="#myCarousel" data-slide-to="6"></li>
	<li data-target="#myCarousel" data-slide-to="7"></li>

</ul>

<div class="carousel-inner">
	<div class="item active">

		<img style="float:left;margin-left:50px" class="img-rounded" src="ChickenFlossGunkan.jpg">

			<img style="float:left;margin-left:50px" class="img-rounded" >
			<div>
				<h2 class="f"> dd</h2>
			</div>
		</div>
	<div class="item">
		<img style="float:left;margin-left:50px" class="img-rounded" src="ChickenFlossGunkan.jpg">
		<div>
			<h2 class="f">Chicken Floss Gunkan</h2>
		</div>
	</div>
	<div class="item">
		<img style="float:left;margin-left:50px"  class="img-rounded" src="BlackCaviarGunkan.jpg">
		<div>
			<h2 class="f">Black Caviar Gunkan</h2>
		</div>
	</div>
	<div class="item">
		<img style="float:left;margin-left:50px" class="img-rounded" src="InariKaniCornMayo.jpg">
		<div>
			<h2 class="f">Inari Kani Corn Mayo</h2>
		</div>
	</div>
	<div class="item">
		<img style="float:left;margin-left:50px" class="img-rounded" src="TamagoGunkan.jpg">
		<div>
			<h2 class="f">Tamago Gunkan</h2>
		</div>
	</div>
	<div class="item">
		<img style="float:left;margin-left:50px" class="img-rounded" src="KaniCheeseGunkan.jpg">
		<div>
			<h2 class="f">Kani Cheese Gunkan</h2>
		</div>
	</div>
	<div class="item">
		<img style="float:left;margin-left:50px" class="img-rounded" src="InariGreenTobiko.jpg">
		<div>
			<h2 class="f">Inari Green Tobiko</h2>
		</div>
	</div>
	<div class="item">
		<img style="float:left;margin-left:50px" class="img-rounded" src="InariBlackCaviar.jpg">
		<div>
			<h2 class="f">Inari Black Caviar</h2>
		</div>
	</div>
</div>
<a class="left carousel-control" href="#myCarousel" data-slide="prev">
	<span class="glyphicon glyphicon-chevron-left"></span></a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
	<span class="glyphicon glyphicon-chevron-right"></span></a>

</div>

</div>

</center>
<br>
<br>
<br>
<br>
<?php
    include("koneksi.php");
    $sql="SELECT * FROM produk LIMIT 4";
    $result=$kon->query($sql);
   if($result->num_rows>0){?>
<div class="container">
	<center>
<div  id="myCarousel2" class="carousel slide" data-ride="stop">
<ul class="carousel-indicators">
<li data-target="#myCarousel2"    class="active"></li>
<li data-target="#myCarousel2" data-slide-to="1"></li>

</ul>
<?php
   while($row=$result->fetch_assoc()){ ?>
<div class="carousel-inner">

<div class="item active">
 <div class="col-xs-3">
	<a data-target="#myCarousel" data-slide-to="0" href="">
	<img style="margin-left:20px;width:200px;height:150px" class="img-rounded" src='./images/<?php echo $row['foto_produk']; ?>' > 
  </div> 
 <div class="col-xs-3">
	<a data-target="#myCarousel" data-slide-to="1" href="">
		<img style="margin-left:0px;width:200px;height:150px" class="img-rounded"  src='./images/<?php echo $row['foto_produk']; ?>'  >
	   </div>  <div class="col-xs-3">
		  <a data-target="#myCarousel" data-slide-to="2" href="">
			<img style="margin-left:0px;width:200px;height:150px" class="img-rounded" src='./images/<?php echo $row['foto_produk']; ?>'  >
		   </div>  <div class="col-xs-3">
			  <a data-target="#myCarousel" data-slide-to="3" href="">
				<img style="margin-left:0px;width:200px;height:150px" class="img-rounded" src='./images/<?php echo $row['foto_produk']; ?>' >
			   </div> 
 
</div>
<a class="left carousel-control" href="#myCarousel2" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" href="#myCarousel2" data-slide="next">
<span class="glyphicon glyphicon-chevron-right"></span>
</a>
<?php }?>
 
 
 <?php
 } else{
	 echo "Tidak dapat menampilkan record";
 }
	 ?>
</div>

</div>
</center> <br><br><br><br>


<?php

include_once("footer.php");

?>