<?php

require_once("../koneksi.php");

$id_admin = $_POST["id_admin"];
$id_kategori = $_POST["id_kategori"];
$kode_produk = $_POST["kode_produk"];
$nama_produk = $_POST["nama_produk"];
$harga_produk = $_POST["harga_produk"];

$target_dir = "./images/";
$namafile = uniqid().basename($_FILES["fileToUpload"]["name"]);
$target_file = $target_dir . $namafile;
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}



// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    $stmt = $kon->prepare("INSERT INTO produk(id_admin,id_kategori,kode_produk,nama_produk,harga_produk,foto_produk) VALUES (?,?,?,?,?,?)");
    $stmt->bind_param("ssssis", $id_admin,$id_kategori,$kode_produk,$nama_produk,$harga_produk,$namafile);
    try{
        $stmt->execute();
        move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
        $pesan = "Produk $nama_produk berhasil ditambahkan.";
        header("Location: ../tampilProduk.php?pesan=$pesan");
    }catch(Exception $e){
        $pesan = "Proses tambah Produk gagal, kesalahan:".$e->getMessage();
        header("Location: /AMJ/admin2/tambahproduk.php?pesan=$pesan");
    }finally {
        $stmt->close();
        $kon->close();
    }
}

?>