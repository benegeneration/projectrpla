<?php
 
require_once("headerpage.php");

include_once("../koneksi.php");
$sql="SELECT * FROM franchise";
$query=mysqli_query($kon,$sql);
?>

   <div class="col-md-10" style="padding:0px">
   <ol class="breadcrumb">
   <li class="breadcrumb-item">
     <a href="index.php">Home</a>
   </li>
   <li class="breadcrumb-item active">Data Franchise</li>
 </ol>
   </div>
 <div class="col-md-10" style="min-height:600px">
         <div class="col-md-12" style="padding:10px; padding-left:0;padding-right:0;">            

            <a href="tambahFranchise.php" class="btn btn-info">Tambah Franchise</a>

         </div>
            <table class="table table-bordered">
               <tr>
               <th class="info">ID Franchise</th>
               <th class="info">Nama Franchise</th>
               <th class="info">Username</th>
               <th class="info">Password Franchise</th>
               <th class="info">Email Franchise</th>
               <th class="info">Alamat Franchise</th>
                  <th class="info">No. Telp</th>
                  <th class="info">Wilayah Franchise</th>
                  <th class="info">ID Admin</th>
                  <th class="info">Jabatan</th>

                  <th class="info" colspan="3">Action</th>
               </tr>

<?php
while($row=mysqli_fetch_array($query)){
?>
<tr>
<td><?php echo $row['id_franchise'];?></td>
<td><?php echo $row['nama_franchise'];?></td>
<td><?php echo $row['username'];?></td>
<td><?php echo $row['passwordfrch'];?></td>
<td><?php echo $row['email'];?></td>
<td><?php echo $row['alamat'];?></td>
<td><?php echo $row['no_telp'];?></td>
<td><?php echo $row['wilayah_franchise'];?></td>
<td><?php echo $row['id_admin'];?></td>
<td><?php echo $row['jabatan'];?></td>


<td>
<a href="ubahFranchise.php?id_franchise=<?php echo $row['id_franchise']; ?>">Detail</a></td>
<td>
<a href="prosesHapusFranchise.php?id_franchise=<?php echo $row['id_franchise']; ?>" onClick="return confirm('Apakah data ingin dihapus?')">Delete </a>
</td>


</tr>

<?php
}

}else{
  header("Location:admin2/Franchise.php");
}

require_once("footerpage.php");
?>
     </table>

</div>         