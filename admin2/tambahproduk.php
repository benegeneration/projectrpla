<?php 
    /*if(!isset($_SESSION["username"])){
        header("Location: /ukdwstore/loginform.php");
    }*/
    require_once("headerpage.php");
?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Tambah Produk</li>
</ol>

<div class="row">
    <div class="col-6">
        <h1>Tambah Produk</h1>
        <?php
                if(isset($_GET["pesan"]) && $_GET["pesan"]!=""){
                    echo "<span class='alert alert-success'>".$_GET["pesan"]."</span>";
                }
            ?><br><br>
        <form action="prosestambahproduk.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="id_admin">ID Admin:</label>
                <input type="text" class="form-control" name="id_admin">
            </div>
            <div class="form-group">
                <label for="id_kategori">ID Kategori:</label>
                <input type="text" class="form-control" name="id_kategori">
            </div>
            <div class="form-group">
                <label for="kode_produk">Kode Produk:</label>
                <input type="text" class="form-control" name="kode_produk">
            </div>
            <div class="form-group">
                <label for="nama_produk">Nama Produk:</label>
                <input type="text" class="form-control" name="nama_produk">
            </div>
            <div class="form-group">
                <label for="harga_produk">Harga Produk:</label>
                <input type="number" class="form-control" name="harga_produk">
            </div>
            <div class="form-group">
                <label for="fileToUpload">Gambar:</label>
                <input type="file" name="fileToUpload">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>

<?php 
    require_once("footerpage.php");
?>