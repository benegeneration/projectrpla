<?php
session_start();
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="logo.jpg">
  <title>Adi Muda Jaya</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-info fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.html"  style="color:black; font-weight:bold">Adi Muda Jaya Group</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
    <?php
        if(isset($_SESSION["username"])){ ?>
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="circle">
          <a class="nav-link" href="index.php">
            <i class="fa fa-fw fa-area-circle"></i>
            <span class="nav-link-text" style=" font-weight:bold;font-size:20px;color:white">Home</span>
          </a>
        </li>
      
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Data">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseData" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text">Data</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseData">
            <li>
              <a href="dataSushi.php">Data SushiGO</a>
            </li>
            <li>
              <a href="dataChio.php">Data Chio</a>
            </li>
            <li>
              <a href="dataChrunchy.php">Data Chrunchy Crab</a>
            </li>
            <li>
              <a href="Franchise.php">Data Franchise</a>
            </li>
            <li>
              <a href="Promo.php">Data Promosi</a>
            </li>
          </ul>
        </li>
      </ul>
        <?php } ?>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
     

       
       <?php if(isset($_SESSION["username"])) { ?>
          <li class="nav-item">
          <a class="nav-link" style="color:black"><?php echo "Welcome: ". $_SESSION["username"]; ?></a>
          </li>
        <li class="nav-item">
          <a class="nav-link" href="../logout.php"  style="color:black">
            <i class="fa fa-fw fa-sign-out"  style="color:black"></i>Logout</a>
        </li>
        <?php } else { ?>
       
          <li class="nav-item">
            <a class="nav-link" href="../index.php"  style="color:black"> 
            <i class="fa fa-fw fa-sign-out"  style="color:black"></i>Login</a>
          </li>
          <li>
          <a class="nav-link bg-info" href="../home1.php"  style="color:black">
            <i    style="color:black"></i>Home AMJ</a>
      
          </li>
        <?php } ?>
    
   
</ul>   

       
    </div>
    
  </nav>
  
  <div class="content-wrapper">
    <div class="container-fluid">
    